var express = require('express');
var requestJson = require('request-json')
var bodyParser=require('body-parser');         //necesario para obtener datos del body
var app = express();

var port = process.env.PORT || 3000;  //Si hay una variable PORT lo toma sino toma el 3000
var URLBase ='/apiperu/v1/';


var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdtechu/collections"
var apiKey = "apiKey=LrJOBRz6HbqJ3ZOO70wiS5neYR6HkfqV"
var clienteMlab

app.use(bodyParser.json());

//GET PERSONAS - login
app.get(URLBase+'login/:email/:password', function(req, res) {
    let nombreUser = req.params.email;
    let passlUser = req.params.password;
    let queryUser='?q={"pers_email":"'+nombreUser+'","password":"'+passlUser+'"}&';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/persona"+queryUser + apiKey)
    clienteMlab.get('', function(err, resM, bodyGPE) {
      if (!err) {
        if (bodyGPE.length == 1){ // Login ok
          res.send(bodyGPE);

          //Se genera tocken
          let tocken= Math.random();
          let tbPersona = '{"$set":{"pers_tocken":"'+tocken+'"}}';
          let tbProducto = '{"$set":{"prod_tocken":"'+tocken+'"}}';

          //Se registra el tocken en la tabla persona
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/persona")
          clienteMlab.put('?q={"_id":{"$oid":"'+ bodyGPE[0]._id.$oid + '"}}&' + apiKey, JSON.parse(tbPersona), function(errPE, resPE, bodyPPE) {
          });
          console.log("tamaño :"+bodyGPE.length);
          console.log(bodyGPE);

          //Se registra el tocken en la tabla producto
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/producto")
          clienteMlab.put('?q={"prod_idCliente":'+ bodyGPE[0].pers_codCliente + '}&m=true&' + apiKey, JSON.parse(tbProducto), function(errPR, resPR, bodyPPR) {
            });


        }else{
          res.send([{"pers_email":"pers_email"}]);
        }
      }else{
        res.status(500).send({"msj":"Error en la peticion"});

      }
    })
});

//PRODUCTOS
//GET
app.get(URLBase+'productos/:idpersona', function(req, res) {
  let idPersona = req.params.idpersona;
  let queryPersona='?q={"_id":{"$oid":"'+ idPersona+'"}}&';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/persona"+queryPersona + apiKey)
    clienteMlab.get('', function(err, resM, bodyGPE) {

      if (!err) {
        if (bodyGPE.length == 1){
          let queryProducto='?q={"prod_idCliente":'+bodyGPE[0].pers_codCliente+',"prod_tocken":"'+bodyGPE[0].pers_tocken+'"}&';
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/producto"+queryProducto + apiKey)
          clienteMlab.get('', function(err, resM, bodyGPR) {
              res.send(bodyGPR);
          });
        }else{
          res.send([{"pers_email":"pers_email"}]);
        }
      }else{
        res.status(500).send({"msj":"Error en la peticion"});
      }
    })
});


//GET movimientos users/:nombre/:apellido
app.get(URLBase+'producto/movimiento/:idsesion/:producto/:movimiento/:cuenta', function(req, res) {
  let idProducto=req.params.producto;
  let idCuenta=req.params.cuenta;
  let idMovimiento=req.params.movimiento;
  console.log(idProducto+idCuenta+":"+idMovimiento);
  let idPersona =req.params.idsesion;

  let queryPersona='?q={"_id":{"$oid":"'+ idPersona+'"}}&';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/persona"+queryPersona + apiKey)
    clienteMlab.get('', function(err, resM, bodyGPE) {

      if (!err) {
        if (bodyGPE.length == 1){
          let queryProducto='?q={"prod_idCliente":'+bodyGPE[0].pers_codCliente+',"prod_tocken":"'+bodyGPE[0].pers_tocken+'","prod_contrato.prod_cont_producto.prod_cont_prod_codigo":"'+idProducto+'","prod_contrato.prod_cont_cuenta":"'+idCuenta+'","prod_movimientos.prod_movi_numero":'+idMovimiento+'}&';
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/producto"+queryProducto + apiKey)
          clienteMlab.get('', function(err, resM, bodyGPR) {
              res.send(bodyGPR);
          });
        }else{
          res.send([{"pers_email":"pers_email"}]);
        }
      }else{
        res.status(500).send({"msj":"Error en la peticion"});
      }
    })
});

// OBTENER PRODUCTOS DE LA TIENDA
app.get(URLBase+'tienda/productos', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/tienda?" + apiKey)
    clienteMlab.get('', function(err, resM, bodyGTI) {
      if (!err) {
        if (bodyGTI.length >= 1){
          res.send(bodyGTI);
        }else{
          res.send([{"sin_datos":"sin_datos"}]);
        }
      }else{
        res.status(500).send({"msj":"Error en la peticion"});
      }
    })
});

//PAGAR
app.get(URLBase+'comprar/:tarjeta/:vencimiento/:codigo/:importe/:moneda/:comercio/:latitud/:longitud', function(req, res) {
  //Se obtienen datos de entrada
  let dTarjeta=req.params.tarjeta;
  let dVencimiento=req.params.vencimiento;
  dVencimiento =dVencimiento.slice(0, 2)+"/"+dVencimiento.slice(2, 4);
  let dCodigo=req.params.codigo;
  var dImporte=req.params.importe;
  var dMoneda=req.params.moneda;
  var dComercio=req.params.comercio;
  var dLatitud=req.params.latitud;
  var dLongitud=req.params.longitud;

  //Query para obtener datos de la tarjeta para la compra
  let queryTarjeta='?q={"prod_tarjeta.prod_tarj_nro":"'+dTarjeta+'","prod_tarjeta.prod_tarj_fechBaja":"'+dVencimiento+'","prod_tarjeta.prod_tarj_ccv":"'+dCodigo+'","prod_tarjeta.prod_tarj_estado.prod_tarj_esta_bloqueo":"N"}&';

    clienteMlab = requestJson.createClient(urlMlabRaiz + "/producto"+queryTarjeta + apiKey)
    clienteMlab.get('', function(err, resM, bodyGPR) {

      if (!err) {
        if (bodyGPR.length == 1){
          var saldoTarjeta =0;
          var saldoContrato =0;
          var saldoDisponible=0;

          //Valida saldos de la tarjeta
          var prodSaldoLimite = bodyGPR[0].prod_saldos.prod_sald_limite;
          var prodSaldoConsumido = bodyGPR[0].prod_saldos.prod_sald_consumido;
          var tarjSaldoLimite = bodyGPR[0].prod_tarjeta.prod_tarj_saldos.prod_tarj_sald_limite;
          var tarjSaldoConsumido = bodyGPR[0].prod_tarjeta.prod_tarj_saldos.prod_tarj_sald_consumido;
          saldoContrato = prodSaldoLimite - prodSaldoConsumido;
          saldoTarjeta  = tarjSaldoLimite - tarjSaldoConsumido;
          if(saldoTarjeta <= saldoContrato){
            saldoDisponible = saldoTarjeta;
          }else{
            saldoDisponible = saldoContrato;
          }

          if (dImporte <= saldoContrato) {
            console.log("saldoTarjeta: "+saldoTarjeta);
            console.log("saldoContrato: "+saldoContrato);

            //Fecha y hora de la operaciòn
            var f=new Date();
            var fechaActual = f.getFullYear()+"-"+(f.getMonth() +1)+"-"+f.getDate();
            var horaActual = f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();

            let updateProducto =bodyGPR[0];

            //Actualiza importes de la tarjeta y contrato
            updateProducto.prod_saldos.prod_sald_consumido= parseInt(updateProducto.prod_saldos.prod_sald_consumido) + parseInt(dImporte);
            updateProducto.prod_tarjeta.prod_tarj_saldos.prod_tarj_sald_consumido= parseInt(updateProducto.prod_tarjeta.prod_tarj_saldos.prod_tarj_sald_consumido) + parseInt(dImporte);

            //Se inserta los datos de la operaciòn
            updateProducto.prod_movimientos[updateProducto.prod_movimientos.length]={
            "prod_movi_numero": updateProducto.prod_movimientos.length + 1,
            "prod_movi_comercio": dComercio,
            "prod_movi_momento": {
                "prod_movi_mome_fecha": fechaActual,
                "prod_movi_mome_hora": horaActual
            },
            "prod_movi_monto": {
                "prod_movi_mont_importe": parseInt(dImporte),
                "prod_movi_mont_moneda": dMoneda,
                "prod_movi_mont_tipoCambio": 1
            },
            "prod_movi_geoLocalizacion": {
                "prod_movi_geol_Latitud": dLatitud,
                "prod_movi_geol_Longitud": dLongitud
            },
            "prod_movi_cuotas": [
                {
                    "prod_movi_cuot_nroCuota": 1,
                    "prod_movi_cuot_importe": parseInt(dImporte),
                    "prod_movi_cuot_moneda": dMoneda,
                    "prod_movi_cuot_descripcion": "Compra en cuotas",
                    "prod_movi_cuot_fechFacturacion": "2019-01-16",
                    "prod_movi_cuot_estado": "Pendiente"
                }
            ]
        };

            //Actualiza datos en la tabla producto
            clienteMlab.put('producto/'+updateProducto._id.$oid+'?'+apiKey,updateProducto, function(err, resM, body) {
              var respuesta =[{}];
              if (err) {
                respuesta=[{"msj":"Error, Vuelva intentarlo."}];
                res.status(500);
              }else{
                respuesta=[{"msj":"Compra realizada con exito."}];
              }
              res.send(respuesta);
            });

          }else{
            res.send([{"msj":"Denegado, saldo insuficiente."}]);
          }
        }else{
          res.send([{"msj":"Error, Datos invàlidos."}]);
        }
      }else{
        res.status(500).send({"msj":"Error de peticion, Vuelva intentarlo."});
      }
    })
});

// OBTENER DATOS DEL CLIENTE
app.get(URLBase+'perfil/:idpersona', function(req, res) {
  let idPersona = req.params.idpersona;
  let queryPersona='?q={"_id":{"$oid":"'+ idPersona+'"}}&';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/persona"+queryPersona + apiKey)
    clienteMlab.get('', function(err, resM, bodyGPE) {

      if (!err) {
        if (bodyGPE.length == 1){
          res.send(bodyGPE);
        }else{
          res.send([{"msj":"Error, Datos invàlidos."}]);
        }
      }else{
        res.status(500).send({"msj":"Error en la peticion"});
      }
    })
});

//Actualiza datos del cliente
app.get(URLBase+'perfil/actualizar/:sessionID/:residTipo/:residDireccion/:residNumero/:email/:celular/:clave/:claveNew', function(req, res) {
  var idPersona =req.params.sessionID;
  var newResidTipo=req.params.residTipo;
  var newResidDireccion=req.params.residDireccion;
  var newResidNumero=req.params.residNumero;
  var newEmail=req.params.email;
  var newCelular=req.params.celular;
  var newClave=req.params.clave;
  var newClaveNew=req.params.claveNew;

  let queryPersona='?q={"_id":{"$oid":"'+ idPersona+'"}}&';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/persona"+queryPersona + apiKey)
    clienteMlab.get('', function(err, resM, bodyGPE) {

      if (!err) {
        if (bodyGPE.length == 1){

            //Valida la clave del cliente
            if(bodyGPE[0].password == newClave){
              var updatePersona = bodyGPE[0];
              updatePersona.pers_lugRecidencia.pers_reci_dire.pers_reci_dire_tipo=newResidTipo;
              updatePersona.pers_lugRecidencia.pers_reci_dire.pers_reci_dire_calle=newResidDireccion;
              updatePersona.pers_lugRecidencia.pers_reci_dire.pers_reci_dire_nume=newResidNumero;
              updatePersona.pers_email=newEmail;
              updatePersona.pers_celular=newCelular;

              //Se valida si la nueva clave ha cambiado
              if(newClaveNew != 'N0'){
                updatePersona.password=newClaveNew;
              }

              //Se actualiza el registro PERSONA
              clienteMlab.put('persona/'+updatePersona._id.$oid+'?'+apiKey,updatePersona, function(err, resM, body) {
                var respuesta =[{}];
                if (err) {
                  respuesta=[{"msj":"Error, Vuelva intentarlo."}];
                  res.status(500);
                }else{
                  respuesta=[{"msj":"Datos actualizados."}];
                }
                res.send(respuesta);
              });

            }else{
              console.log('Clave errada');
              res.send([{"msj":"Error, clave actual invàlida."}]);
            }


        }else{
          console.log('sin datos');
          res.send([{"msj":"Error, datos invàlidos."}]);
        }
      }else{
        console.log("500.");
        res.status(500).send([{"msj":"Error, vuelva a intentarlo."}]);
      }
    })

});


app.listen(3000);
