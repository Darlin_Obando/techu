# Imagen docker base inicial
FROM node:latest

#Crear el directorio de trabajo del contenedor
WORKDIR /docker-api

#Copiar archivos del proyecto en directorio docker
ADD . /docker-api

#Instalar dependencies necesarias del Proyecto en producciòn
#RUN npm install -production
# ò RUN npm install --only=production      (Depende de la versiòn)

#Puerto donde exponemos nuestro contenedor (mismo que definimos en nuestra API)
EXPOSE 3000

#Lanzar la aplicaciòn  (appe.js)
CMD ["npm","run","dev"]
